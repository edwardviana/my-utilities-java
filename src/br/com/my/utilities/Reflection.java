package br.com.my.utilities;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Eduardo
 * Class using java.lang.reflect
 */
public final class Reflection {

	/**
	 * Expect class type
	 */
	private Class<?> object;

	/**
	 * 
	 * @param object class type to reflection
	 */
	public Reflection(Class<?> object) {
		this.object = object;
	}

	/**
	 * 
	 * @param packageName where located class type
	 * @throws ClassNotFoundException
	 */
	public Reflection(String packageName) throws ClassNotFoundException {
		object = Class.forName(packageName);
	}

	/**
	 * 
	 * @return Get all declarated fields from class
	 */
	public List<String> getFields() {
		List<String> fields = new ArrayList<>();
		for (Field field : object.getDeclaredFields()) {
			fields.add(field.getName());
		}
		return fields;
	}

	/**
	 * 
	 * @param value 
	 * @param field
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public void setField(Object value, String field)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Field fieldAcess = object.getClass().getDeclaredField(field);
		fieldAcess.setAccessible(true);
		fieldAcess.set(object, value);
	}

	/**
	 *
	 * @return Get all declarated methods from class
	 */
	public List<String> getMethods() {
		List<String> methods = new ArrayList<>();
		for (Method method : object.getClass().getMethods()) {
			methods.add(method.getName());
		}
		return methods;
	}
}
