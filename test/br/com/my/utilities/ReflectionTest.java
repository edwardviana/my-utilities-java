package br.com.my.utilities;



import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.*;
import br.com.my.entidade.Cliente;

public class ReflectionTest {

	private Cliente cliente;
	private Reflection reflection;
	
	@Before
	public void before() {
		cliente = new Cliente(1L, "teste", 99);
		reflection = new Reflection(cliente.getClass());
	}
	
	@Test
	public void getAllFields_ListNotEmpty() {
		List<String> fields = reflection.getFields();
		assertFalse(fields.isEmpty());
	}
	
	@Test
	public void getAllFields() {
		List<String> actual = reflection.getFields();
		List<String> expected = new ArrayList<>();
		expected.addAll(Arrays.asList("id","nome","idade"));
		assertEquals(expected, actual);
	}
	
//	@Test
//	public void getAllMethods() {
//		List<String> actual = reflection.getMethods();
//		List<String> expected = new ArrayList<>();
//		expected.addAll(Arrays.asList("getId","setId","getNome","setNome","getIdade","setIdade"));
//		assertEquals(expected, actual);
//	}
	
}		
